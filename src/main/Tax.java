package sample;
 
 public class Tax {
	public String test;
 
	public static void main(String[] args) {
		int price = Integer.parseInt(args[0]);
		int consumptionTax = calcConsumptionTax(price);
		System.out.println(consumptionTax);
	}
 
	public static int calcConsumptionTax(int price) {
		return (int) (price * 0.08);
	}
}
